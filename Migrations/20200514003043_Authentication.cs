﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace fileRPG.Migrations
{
    public partial class Authentication : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "userModel",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Username = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Role = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_userModel", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "userModel",
                columns: new[] { "Id", "Password", "Role", "Username" },
                values: new object[] { 1, "QL0AFWMIX8NRZTKeof9cXsvbvu8=", "admin", "Mestre" });

            migrationBuilder.CreateIndex(
                name: "IX_userModel_Username",
                table: "userModel",
                column: "Username",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "userModel");
        }
    }
}
