using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using fileRPG.Data;
using fileRPG.Models;
using fileRPG.Services;

namespace fileRPG.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "admin")]
    public class UserController : ControllerBase
    {
        private readonly ApiContext _context;

        public UserController(ApiContext context)
        {
            _context = context;
        }

        // GET: api/User
        [HttpGet]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetuserModel()
        {
            // return await _context.userModel.ToListAsync();
            return await _context.userModel.Select(u => MapUser(u)).ToListAsync();
        }

        // GET: api/User/5
        [HttpGet("{id}")]
        public async Task<ActionResult<userModel>> GetuserModel(int id)
        {
            var userModel = await _context.userModel.FindAsync(id);

            if (userModel == null)
            {
                return NotFound();
            }
            return MapUser(userModel);
        }

        // PUT: api/User/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutuserModel(int id, userModel userModel)
        {
            if (id != userModel.Id)
            {
                return BadRequest();
            }

            _context.Entry(userModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!userModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/User
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<userModel>> PostuserModel(userModel userModel)
        {
            _context.userModel.Add(userModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetuserModel", new { id = userModel.Id }, MapUser(userModel));
        }

        // DELETE: api/User/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<userModel>> DeleteuserModel(int id)
        {
            var userModel = await _context.userModel.FindAsync(id);
            if (userModel == null)
            {
                return NotFound();
            }

            _context.userModel.Remove(userModel);
            await _context.SaveChangesAsync();

            return userModel;
        }

        private static dynamic MapUser(userModel user) 
        {
            return new
            {
                Id = user.Id,
                Username = user.Username   
            };
        }

        private bool userModelExists(int id)
        {
            return _context.userModel.Any(e => e.Id == id);
        }
    }
}
