using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using fileRPG.Data;
using fileRPG.Models;
using fileRPG.Services;

namespace fileRPG.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RPGfileController : ControllerBase
    {
        private readonly ApiContext _context;

        public RPGfileController(ApiContext context)
        {
            _context = context;
        }

        // GET: api/RPGfile
        [HttpGet]
        public async Task<ActionResult<IEnumerable<fileModel>>> GetfileModel()
        {
            return await _context.fileModel.ToListAsync();
        }

        // GET: api/RPGfile/5
        [HttpGet("{id}")]
        public async Task<ActionResult<fileModel>> GetfileModel(long id)
        {
            var fileModel = await _context.fileModel.FindAsync(id);

            if (fileModel == null)
            {
                return NotFound();
            }

            return fileModel;
        }

        // PUT: api/RPGfile/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutfileModel(long id, fileModel fileModel)
        {
            if (id != fileModel.Id)
            {
                return BadRequest();
            }

            _context.Entry(fileModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!fileModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RPGfile
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<fileModel>> PostfileModel(fileModel fileModel)
        {
            _context.fileModel.Add(fileModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetfileModel", new { id = fileModel.Id }, fileModel);
        }

        // DELETE: api/RPGfile/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<fileModel>> DeletefileModel(long id)
        {
            var fileModel = await _context.fileModel.FindAsync(id);
            if (fileModel == null)
            {
                return NotFound();
            }

            _context.fileModel.Remove(fileModel);
            await _context.SaveChangesAsync();

            return fileModel;
        }

        private bool fileModelExists(long id)
        {
            return _context.fileModel.Any(e => e.Id == id);
        }
    }
}
